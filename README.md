# Interformatos
Bash scripts for change the format of image and audio files between several formats.

## Content
* [Installation.](https://github.com/willecg/interformatos#installation "Installation.")
* [Usage.](https://github.com/willecg/interformatos#usage "Usage")
* [Dependencies.](https://github.com/willecg/interformatos#dependencies "Dependencies")
* [License.](https://github.com/willecg/interformatos#license "License")

## Installation
1. Download de `.zip` file.
2. Install the needed dependencies. (See below for more information.)
3. Search for the script that you need.
4. (Optional) Copy the file to your path.

## Usage
### mp32ogg
mp3 to ogg
`cd` to your mp3 files directory.
Run the script `$ mp32ogg` if is installed on your path,
otherwise `$ <Path/To/Mp32ogg/Directory>/mp32ogg`

### ogg-mp3
ogg to mp3
`cd` to your ogg files directory.
Run the script `$ ogg-mp3` if is installed on your path,
otherwise `$ <Path/To/ogg-mp3/Directory>/ogg-mp3`

### wmatomp3
wma to mp3
`cd` to your wma files directory.
Run the script `$ wmatomp3` if is installed on your path,
otherwise `$ <Path/To/wmatomp3/Directory>/wmatomp3`

### chgimg
#### Description
Change the format of images in a tree directories.

#### Usage
`cd` to your image files directory.
Run the script `$ chgimg [options]` if is installed on your path,
otherwise `$ <Path/To/chgimg/Directory>/chgimg [option]`
```
chgimg [options]

Options:
 -d, --del             Delete the original files.
 -s, --src <extensión> Extention of the original files.
 -o, --obj <extensión> Extention of the final files.
 -r, --recursive       Change files on all subdirectories.

 -h, --help            Show the help.
```
For example from jpeg to png only one directory.
`$ <Path/To/chgimg/Directory>/chgimg --src=jpeg --obj=png`
The same but in all directories and with chgimg in path.
`$ chgimg --src=jpeg --obj=png --recursive`

**Note:** With no options the source is a `.tif` file and the objetive is a `.png` file.

## Dependencies
|Script   |Dependencies                  |
|--------:|:-----------------------------|
|mp32ogg  |oggenc(vorbis-tools) and lame |
|ogg-mp3  |oggdec(vorbis-tools) and lame |
|wmatomp3 |mplayer                       |
|chgimg   |ImageMagick                   |


## License
All the scripts in this repository are under [GNU GPLv3](http://www.gnu.org/licenses/gpl-3.0.html)
