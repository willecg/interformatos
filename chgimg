#!/bin/bash
#
# Change the image format recursively
# Copyright (C) 2016  William Ernesto Cárdenas Gómez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

directorio_inicial=`pwd`
borrar="falso"
origen="tif"
destino="png"
recursivo="false"

# opciones
TEMP=`getopt -o ds:o:hvr --long del,src:,obj:,help,rec,verbose -n 'chgimg' -- "$@"`
eval set -- "$TEMP"

visita_directorios()
{
    local directorio_local=`pwd`
    if [ "${directorio_local}" != "${directorio_inicial}" ]
    then
        echo "Entrando a $directorio_local"
    fi
    for item in *
    do
        if [ -d "$item" ]
        then
            cd "$directorio_local/$item"
            visita_directorios
        fi
    done
    convertir
    if [ "${directorio_local}" != "${directorio_inicial}" ]
    then
        echo "Saliendo de ${directorio_local}"
        cd ..
    fi
}

convertir()
{
    for imagen in *."${origen}"
    do
        if [ "${imagen}" != "*.${origen}" ]
        then
            echo " ${imagen} --> `basename "$imagen" .${origen}`.${destino}"
            convert "${imagen}" "`basename "$imagen" .${origen}`.${destino}"
            if [ $borrar == "true" ]
            then
                rm -v "${imagen}"
            fi
        fi
    done
}

muestra_ayuda()
{
    echo ''
    echo 'Modo de uso:'
    echo ' chgimg [opciones]'
    echo ''
    echo 'Cambia de un formato a otro en una estructura'
    echo 'de directorios de forma recursiva.'
    echo ''
    echo 'Opciones:'
    echo ' -d, --del             Borra el archivo original.'
    echo ' -s, --src <extensión> Extensión del archivo original.'
    echo ' -o, --obj <extensión> Extensión del archivo final.'
    echo ' -r, --recursive       Acceso recursivo a los subdirectorios.'
    echo ''
    echo ' -h, --help            Muestra esta ayuda y sale.'
}

while true
do
    case "$1" in
        -d|--del)
            borrar=true
            shift
            ;;
        -s|--src)
            origen=${2##*.}
            shift 2
            ;;
        -o|--obj)
            destino=${2##*.}
            shift 2
            ;;
        -h|--help)
            muestra_ayuda
            exit 0
            ;;
        -r|--recursive)
            recursivo="true"
            shift
            ;;
        -v|--verbose)
            echo "Change Image Format"
            echo "Version: chgimg-1.0"
            echo
            convert --version
            break
            ;;
        --) 
            shift
            break
            ;;
        *)
            echo "Ejecución con opciones default"
            shift
            ;;
    esac
done

if [ "$recursivo" == "false" ]
then
    convertir
else
    visita_directorios
fi
